|                 |                |
| --------------- | -------------- |
| **License**     | [MIT](LICENSE) |
| **PHP-Version** | 7.3            |
| **App-Version** | 2.1            |

# Application
The application borrows ideas from the [Laravel framework](https://laravel.com/docs/).

# Contribute
See [Contribute](CONTRIBUTE.md)

# Changelog
See [CHANGELOG](CHANGELOG.md)

# Todo
* Dependency injection
    * Dependecy injection for `PersistentCollection`
    * Dependency injection service container implementation
    * Dependency injection instead of `global`
* [Doctrine ORM](https://www.doctrine-project.org/projects/orm.html) implementation --> umständlich?
* Use `Reflection`s for `Model` attributes
* `Model` relation management
* Use `public/` as entry point + .htaccess

[More todos &hellip;](https://dsggit01.dsprodukte.de/haedrich.sebastian/rtv/-/issues)

# Folder structure
```
.
├── classes - class loader entry point
│   ├── Collections - Utility classes for array-like collections
│   ├── Controllers - MVC controllers
│   └── Models - MVC models
├── database
│   ├── seeders - JSON files containing sample data which can be inserted into the database
│   ├── setup - scripts to create the respective database tables
│   |   ├── ddl_development - CREATE TABLE statements for development
│   |   └── ddl_production - CREATE TABLE statements for development
│   └── specs - DDLs for database tables
├── public - web root
└── views - html templates
```

# Key features
## AppController
The `AppController` handles layout and dependency injection.

### Layout
Layouts are views in the sub folder `views/layouts`. The can be served by invoking the `pageLayout()` method
```php
    echo $appController->layoutPage(
        $appController->layoutPage($router->navigate(), 'skeleton', [
            'sidebar' => $cart->cart_button()
        ]), 'app', [ 'home_url' => $router->route('shop.index') ]
    );
```

### Dependency injection service container
The dependency injection pattern is helpful to keep code clean, flexible and less redundant. But the more dependencies you have, it can get out of hand and you have about the same amount of problems &mdash; you just displaced the problem. But the service container manages and injects dependencies half-automatically.

```php    
    $appController = new AppController();
    // add n dependencies
    $appController->add_dependency(RouteController::class, [ 'shop.index' ]);

    // use them later in the code and just inject the AppController
    $appController->get_dependencies([ RouteController::class ])
```

## MVC
### Models
Models come with `static` `all()` and `get()` methods to retrieve all or a single model of the collection.

### Views
Views are located in `views/`. They can be served by using the Controller's `view()` method. Parameters are enclosed by double curly braces. Views don't have additional logic.

### Controller
The `Controller` currently just exposed the `view()` method
