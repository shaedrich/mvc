<?php
    use Dotenv\Dotenv;
    use shaedrich\MVC\Controllers\AppController;
    use Doctrine\DBAL\DriverManager;
    use shaedrich\MVC\Enums\Environment;

    include 'vendor/autoload.php';
    $dotenv = Dotenv::createImmutable(__DIR__);
    $dotenv->load();

    if ($_ENV['ENVIRONMENT'] === Environment::Development) {
        ini_set('display_errors', '1');
        error_reporting(E_ALL);
    }

    $phpVersion = explode('.', phpversion());
    if (floatval($phpVersion[0].'.'.$phpVersion[1]) < 7.3) {
        throw new \RuntimeException('The code relies on php version 7.4+. You currently use '.phpversion());
    }

    $conn = DriverManager::getConnection([
        'dbname' => $_ENV['DATABASE_NAME'],
        'user' => $_ENV['DATABASE_USER'],
        'password' => $_ENV['DATABASE_PASSWORD'],
        'host' => $_ENV['DATABASE_HOST'],
        'driver' => $_ENV['DATABASE_DRIVER'],
        'path' => $_ENV['DATABASE_PATH'],
        'driverOptions' => [
            \PDO::ATTR_ERRMODE => \PDO::ERRMODE_WARNING
        ]
    ]);

    include 'database/setup/'.$_ENV['ENVIRONMENT'].'.php';
    //print_r($conn->executeQuery('SELECT name FROM sqlite_master WHERE type="table";')->fetchAllAssociative());

    if (session_status() !== PHP_SESSION_ACTIVE) {
        session_save_path('session');
        session_start();
        echo 'new session at "'.session_save_path().'"'.PHP_EOL;
        $_SESSION['collections'] = [];
    }

    $appController = new AppController();