<?php
    namespace shaedrich\MVC;

    interface SessionStore
    {
        public function write(string $key, $value);

        public function read(string $key);
    }