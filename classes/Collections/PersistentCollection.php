<?php
    namespace shaedrich\MVC\Collections;

    use shaedrich\MVC\HttpException;
    use shaedrich\MVC\SessionStore;

    class PersistentCollection implements \Countable, CollectionInterface
    {
        private $class;
        private $store;
        private $identifier;

        public function __construct(string $class, $store, string $identifier = 'id')
        {
            $this->class = $class;
            if (!($store instanceof SessionStore)) {
                throw new \Exception('Store must implement SessionStore interface!');
            }
            $this->store = $store;
            if (!array_key_exists($class, $_SESSION['collections'])) {
                $_SESSION['collections'][$class] = [];
            }
            if (!property_exists($class, $identifier)) {
                throw new \Exception('Property cannot be used as colletion identifier since it doesn\'t exist in model!');
            }
            $this->identifier = $identifier;
        }

        public function add(...$items)
        {
            foreach($items as $item) {
                if (!($item instanceof $this->class)) {
                    $e = new HttpException('The collection must be of type '.$this->class.'[]');
                    $e->http_response_code(400);
                    throw $e;
                }
            }
            array_push($_SESSION['collections'][$this->class], ...$items);
        }

        public function all(): array
        {
            return $_SESSION['collections'][$this->class];
        }

        public function sum(string $property): int
        {
            return array_reduce($_SESSION['collections'][$this->class], function($sum, $item) use($property) {
                return $sum + $item->{$property};
            }, 0);
        }

        public function count(): int
        {
            return count($_SESSION['collections'][$this->class]);
        }

        public function contains(int $id): bool
        {
            return array_filter($_SESSION['collections'][$this->class], function($item) use($id) {
                return $item->{$this->identifier} === $id;
            }) === 1;
        }

        public function remove(int $index)
        {
            array_splice($_SESSION['collections'][$this->class], $index, 1);
        }

        public function find(callable $callback)
        {
            foreach($_SESSION['collections'][$this->class] as $item) {
                if ($callback($item)) {
                    return $item;
                }
            }
            return false;
        }

        public function findIndex(callable $callback): int
        {
            foreach($_SESSION['collections'][$this->class] as $index => $item) {
                if ($callback($item)) {
                    return $index;
                }
            }
            return false;
        }
    }