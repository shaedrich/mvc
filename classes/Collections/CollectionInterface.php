<?php
    namespace shaedrich\MVC\Collections;

    interface CollectionInterface
    {
        /**
         * Append one or more items to the collection
         * @param array $items
         */
        public function add(...$items);

        /**
         * Return the whole collection
         * @return array
         */
        public function all(): array;

        /**
         * Sum up the total value of a certain properties in all items of the collection
         * @return int
         */
        public function sum(string $property): int;

        /**
         * Count all entries in the collection
         * @see https://www.php.net/manual/en/countable.count.php PHP: Countable::count - Manual
         */
        public function count(): int;

        /**
         * Checks if an item with a given identifier is part of the collection
         * @param int $id
         * @return bool
         */
        public function contains(int $id): bool;

        /**
         * Removes an item at a given index from the collection
         */
        public function remove(int $index);

        /**
         * Searches for a specific item in the collection
         * @param callable $callback Provides a filter function which should return `true` when an item fits the needs
         */
        public function find(callable $callback);

        /**
         * Searches for a specific item in the collection and returns its index
         * @param callable $callback Provides a filter function which should return `true` when an item fits the needs
         * @return int
         */
        public function findIndex(callable $callback): int;
    }