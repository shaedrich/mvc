<?php
    namespace shaedrich\MVC\Collections;

    use shaedrich\MVC\HttpException;

    class Collection implements \Countable, CollectionInterface
    {
        private $collection = [];
        private $class;
        private $identifier;

        public function __construct(string $class, string $identifier = 'id')
        {
            $this->class = $class;
            if (!property_exists($class, $identifier)) {
                throw new \Exception('Property cannot be used as colletion identifier since it doesn\'t exist in model!');
            }
            $this->identifier = $identifier;
        }

        public function add(...$items)
        {
            foreach($items as $item) {
                if (!($item instanceof $this->class)) {
                    $e = new HttpException('The collection must be of type '.$this->class.'[]');
                    $e->http_response_code(400);
                    throw $e;
                }
            }
            return array_push($this->collection, ...$items);
        }

        public function all(): array
        {
            return $this->collection;
        }

        public function sum(string $property): int
        {
            return array_reduce($this->collection, function($sum, $item) use($property) {
                return $sum + $item->{$property};
            }, 0);
        }

        public function count(): int
        {
            return count($this->collection);
        }

        public function contains(int $id): bool
        {
            return array_filter($this->collection, function($item) use($id) {
                echo "$id === $item->{$this->identifier}".PHP_EOL;
                return $item->{$this->identifier} === $id;
            }) === 1;
            die();
        }

        public function remove(int $index)
        {
            array_splice($this->collection, $index, 1);
        }

        public function find(callable $callback)
        {
            foreach($this->collection as $item) {
                if ($callback($item)) {
                    return $item;
                }
            }
        }

        public function findIndex(callable $callback): int
        {
            foreach($this->collection as $item) {
                if ($callback($item)) {
                    return $item;
                }
            }
        }
    }