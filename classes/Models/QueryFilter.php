<?php
    namespace shaedrich\MVC\Models;

    use Doctrine\DBAL\Query\QueryBuilder;

    class QueryFilter
    {
        public $column;
        public $comparator = '=';
        public $value;
        private $queryBuilder;

        public function __construct(QueryBuilder $queryBuilder, string $column, string $comparator, string $value)
        {
            $this->column = $column;
            $this->comparator = $comparator;
            $this->value = $value;
            $this->queryBuilder = $queryBuilder;
        }

        public static function __callStatic(string $method, array $parameters)
        {
            if ($method === 'create') {
                if (count($parameters) === 3) {
                    return new self($parameters[0], $parameters[1], '=', $parameters[2]);
                } else if (count($parameters) === 4) {
                    return new self(...$parameters);
                }
            }
        }

        public function __toString()
        {
            return $this->column.' '.$this->comparator.' '.$this->queryBuilder->createPositionalParameter($this->value);
        }
    }