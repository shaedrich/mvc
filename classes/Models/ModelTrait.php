<?php
    namespace shaedrich\MVC\Models;

    use Doctrine\DBAL\ParameterType;
    use Doctrine\DBAL\Connection;
    use shaedrich\MVC\HttpException;

    trait ModelTrait
    {
        /**
         * Returns all models of the collection
         * @param array [$filters=[]]
         * @return self[]
         */
        public static function all(Connection $conn, array $filters = []): array
        {
            // @todo replace with dependency injection service container
            global $router;
            /** @var Doctrine\DBAL\Query\QueryBuilder $stmt */
            $stmt = $conn
                ->createQueryBuilder()
                ->select(self::$staticProperties)
                ->from(self::$table);
            if (count($filters)) {
                foreach($filters as $filter) {
                    $stmt->where(QueryFilter::create($stmt, $filter->column, $filter->comperator ?? '=', $filter->value));
                }
            }
            $stmt = $stmt->execute();
            $res = [];
            while($entry = $stmt->fetch()) {
                $res[] = new self($router, $conn, $entry);
            }
            return $res;
        }

        /**
         * Returns a model of the collection with the given id
         * @param int $id
         * @return self
         */
        public static function get(Connection $conn, int $id): self
        {
            // @todo replace with dependency injection service container
            global $router;
            /** @var Doctrine\DBAL\Query\QueryBuilder */
            $queryBuilder = $conn->createQueryBuilder();
            /** @var Doctrine\DBAL\Driver\ResultStatement */
            $stmt = $queryBuilder
                ->select(self::$staticProperties)
                ->from(self::$table)
                ->where('id = '.$queryBuilder->createPositionalParameter($id, ParameterType::INTEGER))
                ->execute();
            $result = $stmt->fetch();
            if ($result === false) {
                print_r($stmt->debugDumpParams());
                $e = new HttpException(get_called_class().' with id '.$id.' not found in '.self::$table);
                $e->http_response_code(404);
                throw $e;    
            }
            return new self($router, $conn, $result);
        }

        /**
         * Returns an array containing all static and dynamic model properties
         * @return array
         */
        public function toArray(): array
        {
            $res = [];
            foreach(array_merge(self::$staticProperties, self::DYNAMIC_PROPETIES) as $property) {
                /*if (array_key_exists($property, $this->formatters())) {
                    $res[$property] = $this->formatters()[$property]($this->{$property});
                } else {*/
                    $res[$property] = $this->{$property};
                /*}*/
            }
            return $res;
        }

        /**
         * Returns model representation for serialization
         * This does not include dependencies
         */
        public function __sleep(): array
        {
            return array_merge(self::$staticProperties, ['formatter', 'router'];
        }
    }