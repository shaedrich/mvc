<?php
    namespace shaedrich\MVC;

    class NativeSessionStore implements SessionStore
    {
        public function write(string $key, $value) {
            $_SESSION['collections'][$key] = $value;
        }

        public function read(string $key) {
            return $_SESSION['collections'][$key];
        }
    }