<?php
    namespace shaedrich\MVC\Enums;

    abstract class Environment /*extends \SplEnum*/ {
        const __default = self::Development;
        
        const Development = 'development';
        const Production = 'production';
    }