<?php
    namespace shaedrich\MVC;

    class HttpException extends \Exception
    {
        private $httpResponseCode = 500;
        // Redefine the exception so message isn't optional
        public function __construct($message, $code = 0, \Throwable $previous = null) {
            // make sure everything is assigned properly
            parent::__construct($message, $code, $previous);
        }
    
        // custom string representation of object
        public function __toString() {
            return __CLASS__ . ": [{$this->code}]: {$this->message}\n";
        }

        /**
         * Sets HTTP response code
         * @param int $code An HTTP error code (4xx..5xx)
         */
        public function http_response_code(int $code) {
            $this->http_response_code = $code;
        }

        /**
         * Gets HTTP response code
         * @return int
         */
        public function get_http_response_code(): int {
            return $this->http_response_code;
        }
    }