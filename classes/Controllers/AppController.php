<?php
    namespace shaedrich\MVC\Controllers;

    use shaedrich\MVC\HttpException;

    class AppController extends Controller
    {
        private $dependencies = [];

        /**
         * Returns a given layout with the given content embedded
         * @param string $content Output of a view
         * @param string [$layout='app'] View from the `views/layouts/` folder
         * @param array $additionalParameters View parameters defined in layout 
         * @return string
         */
        public function layoutPage(string $content, string $layout = 'app', array $additionalParameters = []): string
        {
            return $this->view('layouts/'.$layout, array_merge([
                'content' => $content
            ], $additionalParameters));
        }
    
        /**
         * Store dependency for later use
         * @param string $className Dependency class to store
         * @param array $arguments Arguments to pass to the constructer
         * @throws shaedrich\MVC\HttpException
         */
        public function add_dependency(string $className, array $arguments = [])
        {
            if (in_array($className, $this->dependencies)) {
                $e = new HttpException('Dependency "'.$className.'" already added!');
                $e->http_response_code(400);
                throw $e;
            }
            if (!class_exists($className)) {
                $e = new HttpException('Class doesn\'t exist!');
                $e->http_response_code(404);
                throw $e;
            }

            $this->dependencies[$className] = new $className(...$arguments);
        }

        /**
         * Return dependency class instances if they exist
         * @throws \Exception
         * @return array
         */
        public function get_dependencies(string ...$dependencies): array
        {
            foreach ($dependencies as $dependency) {
                if (!array_key_exists($dependency, $this->dependencies)) {
                    throw new \Exception('Dependency doesn\'t exist!');
                }
            }
            return array_filter($this->dependencies, function($dependency) use ($dependencies) {
                return in_array($dependency, $dependencies);
            }, ARRAY_FILTER_USE_KEY);
        }
    }