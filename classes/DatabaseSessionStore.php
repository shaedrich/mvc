<?php
    namespace shaedrich\MVC;

    class DatabaseSessionStore implements SessionStore
    {
        private $sessionId;

        public function __construct()
        {
            $this->sessionId = time();
        }

        public function write(string $key, $value) {
            // @todo replace with dependency injection service container
            global $conn;
            $queryBuilder = $conn->createQueryBuilder();
            print_r([ $this->sessionId, $key, $value ]);
            echo '
                INSERT INTO
                    session
                VALUES
                    (
                        '.$queryBuilder->createPositionalParameter($this->sessionId).',
                        '.$queryBuilder->createPositionalParameter($key).',
                        '.$queryBuilder->createPositionalParameter($value).'
                    )
                ON CONFLICT(session_id, key) DO UPDATE SET
                    value = '.$queryBuilder->createPositionalParameter($value).'
                WHERE
                    session_id = '.$queryBuilder->createPositionalParameter($this->sessionId).' AND
                    key = '.$queryBuilder->createPositionalParameter($key).'
            ';
            print_r($conn
                ->executeQuery('
                    INSERT INTO
                        session
                    VALUES
                        (
                            :session_id,
                            :key,
                            :value
                        )
                    ON CONFLICT(session_id, key) DO UPDATE SET
                        value = :value
                    WHERE
                        session_id = :session_id AND
                        key = :key
                ', [
                    ':session_id' => $this->sessionId,
                    ':key' => $key,
                    ':value' => $value
                ]));
        }

        public function read(string $key) {
            // @todo replace with dependency injection service container
            global $conn;
            $queryBuilder = $conn->createQueryBuilder();
            return $queryBuilder
                ->select('value')
                ->from('session')
                ->where('session_id', $this->sessionId)
                ->andWhere('key = ' . $queryBuilder->createPositionalParameter($key))
                ->execute()
                ->fetchColumn() ?: null;
        }
    }