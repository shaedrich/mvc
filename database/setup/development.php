<?php
    $files = scandir('database/setup/ddl_development');
    foreach($files as $file) {
        if (!in_array($file, [ '.', '..' ])) {
            $conn->query(file_get_contents('database/setup/ddl_development/'.$file));
        }
    }