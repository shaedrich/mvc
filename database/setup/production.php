<?php
    $files = scandir('database/setup/ddl_production');
    foreach($files as $file) {
        if (!in_array($file, [ '.', '..' ])) {
            $conn->query(file_get_contents('database/setup/ddl_production/'.$file));
        }
    }