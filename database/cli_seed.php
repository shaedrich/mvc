<?php
use Models\ProductItem;
// create_product.php <name>
include 'init.php';
require_once "bootstrap.php";
echo '[SEEDER] Initialized'.PHP_EOL;

$seeder = json_decode(file_get_contents('database/seeders/'.$argv[1].'.json'));
echo '[SEEDER] Loaded seeder at database/seeders/'.$argv[1].'.json. '.count($seeder).' items found'.PHP_EOL;

foreach ($seeder as $i => $seed) {
    echo '[SEEDER] Process item '.$i.' ...'.PHP_EOL;
    $product = null;
    try {
        $product = new ProductItem($router, $seed);
    } catch (\Exception $e) {
        echo '[SEEDER] Warning: '.$e->getMessage().'. Skip'.PHP_EOL;
    }
    
    $entityManager->persist($product);
    $entityManager->flush();
    
    echo "[SEEDER] Created Product with ID " . $product->getId() . "\n";
}

